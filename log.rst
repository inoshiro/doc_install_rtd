作業ログ
========

はじめに
--------
Read the Docsを久しぶりにインストールして、何か変わったところがあるかを確認する。


環境
----
IDCFクラウドに作ったインスタンス上にインストールする。

グローバルIP : 210.140.168.120

インストール
------------
以下のドキュメントに従ってインストールする。

http://read-the-docs.readthedocs.org/en/latest/install.html

.. code-block:: sh

    apt-get install git
    apt-get install build-essential
    apt-get install python-dev
    apt-get install libxml2-dev libxslt1-dev zlib1g-dev
    wget https://bootstrap.pypa.io/get-pip.py
    python get-pip.py
    git clone https://github.com/rtfd/readthedocs.org.git

    cd readthedocs.org
    pip install -r pip_requirements.txt

安いインスタンスだからと思うけど、Cモジュールのコンパイルがおっそい。

セットアップ
------------

.. code-block:: sh

   ./manage.py syncdb
   ./manage.py migrate
   ./manage.py load_data test_data

``./manage.py runserver 0.0.0.0:80`` で起動。


使う
----

http://210.140.168.120/

ユーザ登録する。
本当はSMTPサーバを立てて設定をそこに向ければRegistrationのメールが飛んで、アカウント認証をするながれなんだろうけど、今回はやっていないので強制的にアカウントを有効にする。

アカウント認証されていない状態でログインを試行すると、

    <メールアドレス> に確認メールを送信しました。メールアドレスを確認してください

と表示されてログインできない。

アカウントの有効化
^^^^^^^^^^^^^^^^^^

http://210.140.168.120/admin/account/emailaddress/

メールアドレスのモデルに、Verifiedフラグがある。これをオンにする。

ビルドの失敗
^^^^^^^^^^^^
プロジェクトをインポートしたけどビルドに失敗した。

runserverしたコンソールを見るとエラーが表示されている。これ以前からずっとあるやつっぽい。
コード読んでちゃんと原因を確認しよう。

ここ

.. code-block:: python

    def version_from_slug(slug, version):
        from projects import tasks
        from builds.models import Version
        from tastyapi import apiv2 as api
        if getattr(settings, 'DONT_HIT_DB', True):
            version_data = api.version().get(project=slug, slug=version)['results'][0]
            v = tasks.make_api_version(version_data)
        else:
            v = Version.objects.get(project__slug=slug, slug=version)
        return v

local_settings.py でいくつか設定を上書きしたら、ビルドは走るようになった。
が、以下のエラーで失敗する。

.. code-block:: python

    [25/Oct/2014 22:25:03] ERROR [projects.tasks:106] (Build) [install-read-the-docs:latest] Top-level Build Failure
    Traceback (most recent call last):
      File "readthedocs/projects/tasks.py", line 90, in update_docs
        build_results = build_docs(version, force, pdf, man, epub, dash, search, localmedia)
      File "/usr/local/lib/python2.7/dist-packages/celery/app/task.py", line 329, in __call__
        return self.run(*args, **kwargs)
      File "readthedocs/projects/tasks.py", line 366, in build_docs
        html_builder.append_conf()
      File "readthedocs/doc_builder/backends/sphinx.py", line 94, in append_conf
        rtd_string = template_loader.get_template('doc_builder/conf.py.tmpl').render(rtd_ctx)
      File "/usr/local/lib/python2.7/dist-packages/django/template/loader.py", line 138, in get_template
        template, origin = find_template(template_name)
      File "/usr/local/lib/python2.7/dist-packages/django/template/loader.py", line 131, in find_template
        raise TemplateDoesNotExist(name)


テンプレートファイルが読み込めてないらしい。

設定の変更
----------
最終的にこうしたらとりあえずビルドが動くようになった。

``readthedocs/settings/local_settings.py`` を以下の内容で作成。

.. code-block:: python

    import os

    SITE_ROOT = '/'.join(os.path.dirname(os.path.realpath(__file__)).split('/')[0:-2])

    PRODUCTION_DOMAIN = 'readthedocs.org'
    DONT_HIT_DB = True
    SLUMBER_API_HOST = "http://localhost"
    TEMPLATE_DIRS = (
        '%s/readthedocs/templates/' % SITE_ROOT,
        '%s/readthedocs/doc_builder/templates/' % SITE_ROOT,
    )


